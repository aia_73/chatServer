import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;


class Server extends Thread {
    Socket socket;
    int num;

    public static void main(String args[]) {
        try {
            int i = 0;
            ServerSocket server = new ServerSocket(3128);
            System.out.println("server is started");
            while (true) {
                new Server(i, server.accept());
                i++;
            }
        } catch (Exception e) {
            System.out.println("init error: " + e);
        }
    }

    public Server(int num, Socket socket) {
        this.num = num;
        this.socket = socket;
        setDaemon(true);
        setPriority(NORM_PRIORITY);
        start();
    }

    public void run() {
        try {
            while (!this.socket.isClosed()) {
                InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();
                byte buf[] = new byte[100 * 1024];
                int r = inputStream.read(buf);
                String query = new String(buf, 0, r);
                System.out.println(query);
                String[] queryArray = query.split(" ");

                switch (queryArray[0]) {
                    case "getComandsList":
                        getListCommands(outputStream);
                        break;
                    case "getFileList":
                        getFileList(outputStream);
                        break;
                    case "getInfo":
                        getInfo(queryArray,outputStream);
                        break;
                    case "putPhoto":
                        putFile(queryArray, outputStream);
                        break;
                    default:
                        isNotCommand(outputStream);
                }
            }
        } catch (Exception e) {
            System.out.println("init error: " + e);
        }
    }

    private void isNotCommand(OutputStream outputStream) throws IOException {
        String str = "inputStream not command";
        outputStream.write(str.getBytes());
    }

    private void getListCommands(OutputStream outputStream) throws IOException {
        String infoFile = " getComandsList  \n" +
                "•  interupt  name\n" +
                "•  putFile размер_буфера name_file\n" +
                "•  getFileList\n" +
                "•  getInfo name\n" +
                "•  getFile name  (в  ответ от  сервера  строка  что  файл есть  второй  параметр  размер  файла  )";
        outputStream.write(infoFile.getBytes());
    }

    private void getFileList(OutputStream outputStream) throws IOException {
        URL url = getClass().getResource("");
        String infoFile = "";
        File file = new File(url.getPath());
        for (File f : file.listFiles()) {
            if (f.isFile()) {
//                BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
//                infoFile += f.getName() + " " + attr.size() + " байт  " + attr.creationTime() + "    ";
                infoFile += f.getName();
            }
        }
        outputStream.write(infoFile.getBytes());
    }

    private void getInfo(String[] queryArray, OutputStream outputStream) throws IOException {
        URL url = getClass().getResource("");
        String infoFile = "";
        File file = new File(url.getPath());
        for (File f : file.listFiles()) {
            if (f.isFile()) {
                if (queryArray[1].equals(f.getName())) {
                    BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
                    infoFile = f.getName() + " " + attr.size() + " байт  " + attr.creationTime() + "    ";
                    outputStream.write(infoFile.getBytes());
                    return;
                }
            }
        }
        infoFile = "Файл не неайден";
        outputStream.write(infoFile.getBytes());
    }

    private void putFile(String[] queryArray, OutputStream outputStream) throws  IOException {
        byte[] bytes = queryArray[1].getBytes();

        outputStream.write(bytes);
    }

    private void readFile(OutputStream outputStream) throws IOException {
        String str = "Введите текст:";
        outputStream.write(str.getBytes());
    }


    private void setTextFile(String[] queryArray, OutputStream outputStream) throws IOException {
        URL url = getClass().getResource("");
        File file = new File(url.getPath() + queryArray[1]);
        if (!file.exists()) {
            file.createNewFile();
        }
        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
        try {
            for (int i = 2; i < queryArray.length; i++) {
                out.print(queryArray[i] + " ");
            }
        } finally {
            out.close();
        }
        String str = "Текст записан в файл";
        outputStream.write(str.getBytes());
    }

    private void commandBye(String[] queryArray, OutputStream outputStream) throws IOException {
        String str = queryArray[0] + " variant " + queryArray[1];
        outputStream.write(str.getBytes());
        socket.close();
    }

    private void commandHello(String[] queryArray, OutputStream outputStream) throws IOException {
        String str = queryArray[0] + " variant " + queryArray[1];
        outputStream.write(str.getBytes());
    }
}
